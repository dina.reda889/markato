<?php

namespace App\Http\Controllers\API;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Reviews;
use Illuminate\Http\Request;

class ApiReviewsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*
        * Using Model to Display All data Belongs to database
        * return response Json with data
        */
        $Reviews = Reviews::all()->toArray();
        $data = array(
                'success' => true,
                'data' => $Reviews,
                'message' => 'all Reviews Data'
                );
             return response()->json( $data );
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        * Using Model to Store Request data to  database that Belongs to this Model
        * return response Json  Messsage for Save or It fails
        */
        try {
            $Reviews= new Reviews([
           'products_id'=>$request->products_id,
           'reviews'=>$request->reviews,
           'product_rate'=>$request->product_rate,
           'status'=>$request->status,
           'users_id'=>$request->users_id,
                ]);
            $Reviews->save();
            //// retrun Jeson With data that saved and Message With Success
            $data = array(
                'success' => true,
                'data' => $Reviews,
                'message' => 'Review saved Success'
                );
             return response()->json( $data );
        } catch (Exception $e) {
            Log::critical("can not save Review :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        * Using Model to Find  A specific field
        * return response Json  Messsage for found or It fails
        */
        try {
             $Reviews = Reviews::find($id);
             if(!$Reviews){
             $data = array(
                'success' => false,
                'message' => 'This ID Doesnot Exist'
                );
             return response()->json( $data );
             }
             $data = array(
                'success' => true,
                'data' => $Reviews,
                'message' => 'Reviews Founded Success'
                );
             return response()->json( $data );
            
        } catch (Exception $e) {
             Log::critical("can not find Reviews :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        * Using Model to update  A specific field
        * return response Json  Messsage for Updated or It fails
        */

        try {
          $Reviews = Reviews::find($id);
             if(!$Reviews){
                return response()->json(['This ID Not Exist .. !'],404);
             }else{
                 $input = $request->all();
                 $Reviews = Reviews::findorfail($id);
                 $updateNow = $Reviews->update($input);
            return response()->json(['status'=>true ,'Reviews Updated Success .. !'],200);
              }
        } catch (Exception $e) {
            Log::critical("cannot Updated Reviews:{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        * Using Model to Delete  A specific field
        * return response Json  Messsage for Deleted or It fails
        */
        try {
              $Reviews = Reviews::find($id);
             if(!$Reviews){
                return response()->json(['This ID Not Exist .. !'],404);
             }
              $Reviews->delete();
            return response()->json(['Reviews Deleted Success .. '],200);
        } catch (Exception $e) {
             Log::critical("can not find Reviews :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }
}
