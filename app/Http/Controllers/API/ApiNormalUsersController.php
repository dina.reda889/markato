<?php

namespace App\Http\Controllers\API;
use Illuminate\Routing\Controller as BaseController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ApiNormalUsersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*
        * Using Model to Display All data Belongs to database
        * return response Json with data
        */
        $User = User::all()->toArray();
        $data = array(
                'success' => true,
                'data' => $User,
                'message' => 'all Users Data'
                );
             return response()->json( $data );
         

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
          * Using Model to Store Request data to  database that Belongs to this Model
          * return response Json  Messsage for Save or It fails
        */

        try {

            /* base64_decode() --- > with request image 
            * Convert image base64 and Save it by TimeStamp with specific Path to Uploaded It
            */
             if(isset($request->user_photo) && !empty($request->user_photo)){
            $images = base64_decode($request->user_photo);
            $image_name= ''.time().'.png';
            $path = public_path() . "/uploads/2017-06/" . $image_name;
            file_put_contents($path, $images);
            $input_image = "uploads/2017-06/" . $image_name;
	
            }else{
              $input_image = "" ;
            }
            /*
            * Add a New Product at database with Request data 
            */
            // Token Generator
            $token =str_random(60);
            ////////////////////////////////
            $User= new User([
            'name'=>  $request->name,
            'email'=> $request->email,
            'password'=> Hash::make($request->password),
            'phone_number'=>$request->phone_number,
            'user_photo'=> $input_image,
            'remember_token'=> $token ,
                    ]);

            $User->save();
             //// retrun Jeson With data that saved and Message With Success
            $data = array(
                'success' => true,
                'data' => $User,
                'message' => 'User saved Success'
                );
             return response()->json( $data );

        } catch (Exception $e) {
            Log::critical("can not save User :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        * Using Model to Find  A specific field
        * return response Json  Messsage for found or It fails
        */
        try {
             $User = User::find($id);
             if(!$User){
             $data = array(
                'success' => false,
                'message' => 'This ID Doesnot Exist'
                );
             return response()->json( $data );
             }
             $data = array(
                'success' => true,
                'data' => $User,
                'message' => 'User Founded Success '
                );
             return response()->json( $data );
            
        } catch (Exception $e) {
             Log::critical("can not find Products:{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        * Using Model to update  A specific field
        * return response Json  Messsage for Updated or It fails
        */

        try {

             $User = User::findorfail($id);
             if(!$User){
             $data = array(
                'success' => false,
                'message' => 'This ID Doesnot Exist'
                );
             return response()->json( $data );
             }else{
                 if(isset($request->user_photo) && !empty($request->user_photo)){

                    $images = base64_decode($request->user_photo);
                    $image_name= ''.time().'.png';
                    $path = public_path() . "/uploads/2017-06/" . $image_name;
                    file_put_contents($path, $images);
                     /* Check If The request  Null or Not And Save the old data*/
                    if(isset($request->name) && !empty($request->name)){
                         $name = $request->name;
                    }else{
                          $name = $User["name"];
                    }
                    /////////////////////////////////////////// Continue .. 
                    if(isset($request->email) && !empty($request->email)){
                         $email = $request->email;
                    }else{
                          $email = $User["email"];
                    }
                    //////////////////////////////////////////////////// Continue .. 
                   if(isset($request->password) && !empty($request->password)){
                         $password = Hash::make($request->password);
                    }else{
                          $password = $User["password"];
                    }
                    //////////////////////////////////////////////////// Continue .. 
                    if(isset($request->phone_number) && !empty($request->phone_number)){
                         $phone_number = $request->phone_number;
                    }else{
                          $phone_number = $User["phone_number"];
                    }
                   //////////////////////////////////////////////////// Continue .. 
                    /* End Check oF Requests */
                    $input= [
                        'name'=>  $name,
                        'email'=> $email,
                        'password'=> $password,
                        'phone_number'=>$phone_number,
                        'user_photo'=> $input_image,
                        'remember_token'=> $User["remember_token"] ,
                         ];
                $updateNow = $User->update($input);
                }else{
                 $input = $request->all();
                 $User = User::findorfail($id);
                 $updateNow = $User->update($input);
                }
                return response()->json(['status'=>true ,'User Updated Success .. !'],200);
             }
        } catch (Exception $e) {
            Log::critical("can not Updated User :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        * Using Model to Delete  A specific field
        * return response Json  Messsage for Deleted or It fails
        */
        try {
              $User = User::find($id);
             if(!$User){
                return response()->json(['This ID DoesNot Exist .. !'],404);
             }
              $User->delete();
            return response()->json(['User Deleted Success .. '],200);
        } catch (Exception $e) {
             Log::critical("can not find User :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /*
    * Login Users
    */

    public function login(Request $request)
    {
        /*
        * Check for login Users By using Email OR phone_number with the correct Password 
        * if not of this true Return User Not Found 
        * if True Return User Data With Message User Login Sucess
        */
        
           /* 1- check for phone_number And Password */
        if(isset($request->phone_number) && !empty($request->phone_number) 
            && isset($request->password) && !empty($request->password) )
            $user = User::where('phone_number','=',"$request->phone_number")->where('password','=',Hash::make($request->password))->first();
              /* 2- check for Email And Password */
        elseif(isset($request->email) && !empty($request->email) && isset($request->password) && !empty($request->password) )
             $user = User::where('email','=',"$request->email")->where('password','=',Hash::make($request->password))->first();
              /* 3- check for phone_number without  Password */
        if(isset($request->phone_number) && !empty($request->phone_number))
           $user = User::where('phone_number','=',"$request->phone_number")->first();
           /* 4- check for email without  Password */
         elseif(isset($request->email) && !empty($request->email))
           $user = User::where('email','=',"$request->email")->first();
            /* 5 - final result if has found User by using this data*/
        if(!empty($user) && isset($request->password) && !empty($request->password) ){
            if(! Hash::check($request->password, $user->password))
                return response()->json(['User not found .. !'],404);
          }

        if (empty($user)) {
         return response()->json(['User not found .. !'],404);
        }
             $data = array(
                'success' => true,
                'data' => $user,
                'message' => 'Users Login successfully'
                );
         return response()->json( $data );
    }
    /*
    * Login Users
    */
    public function logout(Request $request)
    {
        if ( !isset( $request->id ) )
            return response()->json(['the request can\'t be empty.'],404);

        $user = User::where('id', $request->id)->first();
        if ( empty( $user ) )
            return response()->json(['User Doesn\'t exist'],404);
        try {
            return response()->json(['User logged out successfully'],200);
        }catch(Exception $e){
            Log::critical("Cannot logged out user:{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('Can not logged out the user .. !',500);
        }
    }


}
