<?php

namespace App\Http\Controllers\API;
use Illuminate\Routing\Controller as BaseController;
use App\Models\HomeSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ApiHomeSliderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*
        * Using Model to Display All data Belongs to database
        * return response Json with data
        */
        $slider = HomeSlider::all()->toArray();
        return response()->json($slider);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
          * Using Model to Store Request data to  database that Belongs to this Model
          * return response Json  Messsage for Save or It fails
        */

        try {
        	
            /* base64_decode() --- > with request image 
            * Convert image base64 and Save it by TimeStamp with specific Path to Uploaded It
            */
        	$images = base64_decode($request->input('photo'));
            $image_name= ''.time().'.png';
            $path = public_path() . "/uploads/2017-06/" . $image_name;
            file_put_contents($path, $images);
            /*
            * Add a New Product at database with Request data 
            */
            $slider= new HomeSlider([
            'title'=>$request->input('title'),
            'photo'=> "uploads/2017-06/" . $image_name,
                    ]);
            $slider->save();
            return response()->json(['status'=>true ,'Image saved Success .. !'],200);
        } catch (Exception $e) {
            Log::critical("can not save Image :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        * Using Model to Find  A specific field
        * return response Json  Messsage for found or It fails
        */
        try {
             $slider = HomeSlider::find($id);
             if(!$slider){
                return response()->json(['This ID Doesnot Exist .. !'],404);
             }
             return response()->json($slider,200);
            
        } catch (Exception $e) {
             Log::critical("can not find Image :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        * Using Model to update  A specific field
        * return response Json  Messsage for Updated or It fails
        */

        try {

             $slider = HomeSlider::findorfail($id);
             if(!$slider){
                return response()->json(['This ID Not Exist .. !'],404);
             }else{
                
             	if($request->input('photo')){

             		$images = base64_decode($request->input('photo'));
                    $image_name= ''.time().'.png';
                    $path = public_path() . "/uploads/2017-06/" . $image_name;
                    file_put_contents($path, $images);

                    /* Check If The request  Null or Not And Save the old data*/
                    if($request->input('title')){
                         $title = $request->input('title');
                    }else{
                          $title = $slider["title"];
                    }
                    /* End Check oF Requests */
                    $input=[
                    'title'=> $title,
                    'photo'=> "uploads/2017-06/" . $image_name,
                         ];
                $updateNow = $slider->update($input);
             	}else{
                 $input = $request->all();
                 $slider = HomeSlider::findorfail($id);
                 $updateNow = $slider->update($input);
             	}
                return response()->json(['status'=>true ,'Image Updated Success .. !'],200);
             }
        } catch (Exception $e) {
            Log::critical("can not save Image :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        * Using Model to Delete  A specific field
        * return response Json  Messsage for Deleted or It fails
        */
        try {
              $slider = HomeSlider::find($id);
             if(!$slider){
                return response()->json(['This ID DoesNot Exist .. !'],404);
             }
              $slider->delete();
            return response()->json(['Image Deleted .. '],200);
        } catch (Exception $e) {
             Log::critical("can not find Image :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }
}
