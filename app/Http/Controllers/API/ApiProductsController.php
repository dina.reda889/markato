<?php

namespace App\Http\Controllers\API;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ApiProductsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*
        * Using Model to Display All data Belongs to database
        * return response Json with data
        */
        $products =Products::all()->toArray();
        return response()->json($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
          * Using Model to Store Request data to  database that Belongs to this Model
          * return response Json  Messsage for Save or It fails
        */

        try {
        	
            /* base64_decode() --- > with request image 
            * Convert image base64 and Save it by TimeStamp with specific Path to Uploaded It
            */
        	$images = base64_decode($request->photo);
            $image_name= ''.time().'.png';
            $path = public_path() . "/uploads/2017-06/" . $image_name;
            file_put_contents($path, $images);

            /*
            * Add a New Product at database with Request data 
            */

            $products= new Products([
            'name'=>$request->name,
            'description'=>$request->description,
            'categories_id'=>$request->categories_id,
            'price'=>$request->input('price'),
            'photo'=> "uploads/2017-06/" . $image_name,
            'feature'=>$request->feature,
                    ]);

            $products->save();
            return response()->json(['status'=>true ,'Products saved Success .. !'],200);
        } catch (Exception $e) {
            Log::critical("can not save Products :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        * Using Model to Find  A specific field
        * return response Json  Messsage for found or It fails
        */
        try {
             $products = Products::find($id);
             if(!$products){
                return response()->json(['This ID Doesnot Exist .. !'],404);
             }
             return response()->json($products,200);
            
        } catch (Exception $e) {
             Log::critical("can not find Products :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
  public function update(Request $request, $id)
    {
        /*
        * Using Model to update  A specific field
        * return response Json  Messsage for Updated or It fails
        */

        try {

             $Products = Products::findorfail($id);
             if(!$Products){
                return response()->json(['status'=>false ,'This ID Not Exist .. !'],404);
             }else{
                if($request->input('photo')){

                    $images = base64_decode($request->input('user_photo'));
                    $image_name= ''.time().'.png';
                    $path = public_path() . "/uploads/2017-06/" . $image_name;
                    file_put_contents($path, $images);
                     /* Check If The request  Null or Not And Save the old data*/
                    if($request->input('name')){
                         $name = $request->input('name');
                    }else{
                          $name = $Products["name"];
                    }
                    /////////////////////////////////////////// Continue .. 
                    if($request->input('description')){
                         $description = $request->input('description');
                    }else{
                          $description = $Products["description"];
                    }
                    //////////////////////////////////////////////////// Continue .. 
                   if($request->input('categories_id')){
                         $categories_id = $request->input('categories_id');
                    }else{
                          $categories_id = $Products["categories_id"];
                    }
                    //////////////////////////////////////////////////// Continue .. 
                    if($request->input('price')){
                         $price = $request->input('price');
                    }else{
                          $price = $Products["price"];
                    }
                   //////////////////////////////////////////////////// Continue .. 
                     if($request->feature){
                         $price = $request->feature;
                    }else{
                          $price = $Products["feature"];
                    }
                    /* End Check oF Requests */
                    $input= [
                        'name'=>  $name,
                        'description'=> $description,
                        'categories_id'=> $categories_id,
                        'price'=>$price,
                        'photo'=> $input_image,
                        'feature'=>$request->feature,
                         ];
                $updateNow = $Products->update($input);
                }else{
                 $input = $request->all();
                 $Products = Products::findorfail($id);
                 $updateNow = $Products->update($input);
                }
                return response()->json(['status'=>true ,'Products Updated Success .. !'],200);
             }
        } catch (Exception $e) {
            Log::critical("can not Updated Products :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        * Using Model to Delete  A specific field
        * return response Json  Messsage for Deleted or It fails
        */
        try {
              $products = Products::find($id);
             if(!$products){
                return response()->json(['This ID DoesNot Exist .. !'],404);
             }
              $products->delete();
            return response()->json(['Products Deleted .. '],200);
        } catch (Exception $e) {
             Log::critical("can not find products :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }
}
