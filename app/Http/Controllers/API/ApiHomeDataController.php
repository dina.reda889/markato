<?php

namespace App\Http\Controllers\API;
use Illuminate\Routing\Controller as BaseController;
use App\Models\HomeData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ApiHomeDataController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*
        * Using Model to Display All data Belongs to database
        * return response Json with data
        */
        $HomeData = HomeData::all()->toArray();
        return response()->json($HomeData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
          * Using Model to Store Request data to  database that Belongs to this Model
          * return response Json  Messsage for Save or It fails
        */

        try {
        	
            /* base64_decode() --- > with request image 
            * Convert image base64 and Save it by TimeStamp with specific Path to Uploaded It
            */
        	$images = base64_decode($request->input('company_logo'));
            $image_name= ''.time().'.png';
            $path = public_path() . "/uploads/2017-06/" . $image_name;
            file_put_contents($path, $images);
            /*
            * Add a New Product at database with Request data 
            */
            $HomeData= new HomeData([
            'company_name'=>$request->input('company_name'),
            'company_address'=>$request->input('company_address'),
            'company_facebookAccount'=>$request->input('company_facebookAccount'),
            'company_twitterAccount'=>$request->input('company_twitterAccount'),
            'company_whatsappAccount'=>$request->input('company_whatsappAccount'),
            'company_email'=>$request->input('company_email'),
            'company_phoneNumber'=>$request->input('company_phoneNumber'),
            'company_logo'=> "uploads/2017-06/" . $image_name,
                    ]);
            $HomeData->save();
            return response()->json(['status'=>true ,'HomeData saved Success .. !'],200);
        } catch (Exception $e) {
            Log::critical("can not save HomeData :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        * Using Model to Find  A specific field
        * return response Json  Messsage for found or It fails
        */
        try {
             $HomeData = HomeData::find($id);
             if(!$HomeData){
                return response()->json(['status'=>false ,'This ID Doesnot Exist .. !'],404);
             }
             return response()->json($HomeData,200);
            
        } catch (Exception $e) {
             Log::critical("can not find Data :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        * Using Model to update  A specific field
        * return response Json  Messsage for Updated or It fails
        */

        try {

             $HomeData = HomeData::findorfail($id);
             if(!$HomeData){
                return response()->json(['status'=>false ,'This ID Not Exist .. !'],404);
             }else{
             	if($request->input('company_logo')){

             		$images = base64_decode($request->input('company_logo'));
                    $image_name= ''.time().'.png';
                    $path = public_path() . "/uploads/2017-06/" . $image_name;
                    file_put_contents($path, $images);
                     /* Check If The request  Null or Not And Save the old data*/
                    if($request->input('company_name')){
                         $company_name = $request->input('company_name');
                    }else{
                          $company_name = $HomeData["company_name"];
                    }
                    /////////////////////////////////////////// Continue .. 
                    if($request->input('company_address')){
                         $company_address = $request->input('company_address');
                    }else{
                          $company_address = $HomeData["company_address"];
                    }
                    //////////////////////////////////////////////////// Continue .. 
                   if($request->input('company_facebookAccount')){
                         $company_facebookAccount = $request->input('company_facebookAccount');
                    }else{
                          $company_facebookAccount = $HomeData["company_facebookAccount"];
                    }
                    //////////////////////////////////////////////////// Continue .. 
                    if($request->input('company_twitterAccount')){
                         $company_twitterAccount = $request->input('company_twitterAccount');
                    }else{
                          $company_twitterAccount = $HomeData["company_twitterAccount"];
                    }
                   //////////////////////////////////////////////////// Continue .. 
                    if($request->input('company_whatsappAccount')){
                         $company_whatsappAccount = $request->input('company_whatsappAccount');
                    }else{
                          $company_whatsappAccount = $HomeData["company_whatsappAccount"];
                    }
                   //////////////////////////////////////////////////// Continue .. 
                    if($request->input('company_email')){
                         $company_email = $request->input('company_email');
                    }else{
                          $company_email = $HomeData["company_email"];
                    }
                   //////////////////////////////////////////////////// Continue ..
                    if($request->input('company_phoneNumber')){
                         $company_phoneNumber = $request->input('company_phoneNumber');
                    }else{
                          $company_phoneNumber = $HomeData["company_phoneNumber"];
                    }
                    /* End Check oF Requests */
                    $input= [
                    'company_name'=>$company_name,
                    'company_address'=>$company_address,
                    'company_facebookAccount'=>$company_facebookAccount,
                    'company_twitterAccount'=>$company_twitterAccount,
                    'company_whatsappAccount'=>$company_whatsappAccount,
                    'company_email'=>$company_email,
                    'company_phoneNumber'=>$company_phoneNumber,
                    'company_logo'=> "uploads/2017-06/" . $image_name,
                         ];
                $updateNow = $HomeData->update($input);
             	}else{
                 $input = $request->all();
                 $HomeData = HomeData::findorfail($id);
                 $updateNow = $HomeData->update($input);
             	}
                return response()->json(['status'=>true ,'HomeData Updated Success .. !'],200);
             }
        } catch (Exception $e) {
            Log::critical("can not Updated HomeData :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        * Using Model to Delete  A specific field
        * return response Json  Messsage for Deleted or It fails
        */
        try {
              $HomeData = HomeData::find($id);
             if(!$HomeData){
                return response()->json(['status'=>false ,'This ID DoesNot Exist .. !'],404);
             }
              $HomeData->delete();
            return response()->json(['status'=>true ,'Data Deleted .. '],200);
        } catch (Exception $e) {
             Log::critical("can not find HomeData :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }
}
