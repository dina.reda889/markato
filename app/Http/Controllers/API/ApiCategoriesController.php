<?php

namespace App\Http\Controllers\API;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Categories;
use Illuminate\Http\Request;

class ApiCategoriesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*
        * Using Model to Display All data Belongs to database
        * return response Json with data
        */
        $categories =Categories::all()->toArray();
        return response()->json($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        * Using Model to Store Request data to  database that Belongs to this Model
        * return response Json  Messsage for Save or It fails
        */
        try {
            $categories= new Categories([
           'name'=>$request->input('name'),
           'slug'=>$request->input('slug'),
                ]);
              $categories->save();
            return response()->json(['status'=>true ,'categories saved Success .. !'],200);
        } catch (Exception $e) {
            Log::critical("can not save categories :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        * Using Model to Find  A specific field
        * return response Json  Messsage for found or It fails
        */
        try {
             $categories = Categories::find($id);
             if(!$categories){
                return response()->json(['This ID Not Exist .. !'],404);
             }
            return response()->json($categories,200);
            
        } catch (Exception $e) {
             Log::critical("can not find categories :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        * Using Model to update  A specific field
        * return response Json  Messsage for Updated or It fails
        */

        try {
          $categories = Categories::find($id);
             if(!$categories){
                return response()->json(['This ID Not Exist .. !'],404);
             }else{
                 $input = $request->all();
                 $categories = Categories::findorfail($id);
                 $updateNow = $categories->update($input);
            return response()->json(['status'=>true ,'categories Updated Success .. !'],200);
              }
        } catch (Exception $e) {
            Log::critical("can not save categories :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApiCategories  $apiCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        * Using Model to Delete  A specific field
        * return response Json  Messsage for Deleted or It fails
        */
        try {
              $categories = Categories::find($id);
             if(!$categories){
                return response()->json(['This ID Not Exist .. !'],404);
             }
              $categories->delete();
            return response()->json(['Categories Deleted .. '],200);
        } catch (Exception $e) {
             Log::critical("can not find categories :{$e->getCode()}, {$e->getLine()},{$e->getMessage()}");
            return response('SomeThing Bad .. !',500);
        }
    }
}
