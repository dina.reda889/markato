<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $user =  User::where('id',$id)->first();
         
         return view('home', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
          $user =  User::where('id',$id)->first();
         
         return view('home', ['edit_user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    
        $User = User::where('id',$id)->first();
           if(!$User){
            return redirect('/edit_profile/'.$id)->withErrors('this ID not Exist');
             }else{
               if(isset($request->photo)){
                  $file = $request->file('photo');
                  $destinationPath = public_path(). '/uploads/2017-06/';
                  $filename = time() . '.' . $file->getClientOriginalExtension();
                  $file->move($destinationPath, $filename);
                  $photo  = 'uploads/2017-06/'.$filename;
               }else{
                 $photo = $User->photo;
              }

              if(isset($request->password)){
                 $password =  Hash::make($request->password);
               }else{
                 $password = $User->password;
              }
                    $input= [
                        'name'=>  $request->name,
                        'email'=> $request->email,
                        'password'=> $password,
                        'user_photo'=>$photo,
                        'phone_number'=>$request->phone_number,
                        'facebook_id'=>$User->facebook_id,
                        'points'=>$User->points,
                        'remember_token'=>$request->_token,
                         ];
                $updateNow = $User->update($input);
                
              return redirect('/edit_profile/'.$id)->withMessage('User Data Updated Success .. ');
             }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
