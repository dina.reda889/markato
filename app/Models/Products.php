<?php

namespace  App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    /*Table with this Model 
    * fields with dataBase
    */
      public $table = 'products';
      public $fillable = ['name','description','price','photo','categories_id'];
}
