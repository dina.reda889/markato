<?php

namespace  App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeData extends Model
{
    /*Table with this Model 
    * fields with dataBase
    */
       public $table = 'home_data';
       public $fillable = ['company_name','company_address','company_logo','company_facebookAccount',
                           'company_twitterAccount','company_whatsappAccount','company_email','company_phoneNumber'];
}
