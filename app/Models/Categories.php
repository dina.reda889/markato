<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    /*Table with this Model 
    * fields with dataBase
    */
       public $table = 'categories';
       public $fillable = ['name','slug'];
}
