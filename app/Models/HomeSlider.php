<?php

namespace  App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeSlider extends Model
{
   /*Table with this Model 
    * fields with dataBase
    */
       public $table = 'home_slider';
       public $fillable = ['title','photo'];
}
