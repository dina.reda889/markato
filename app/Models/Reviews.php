<?php

namespace  App\Models;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    /*Table with this Model 
    * fields with dataBase
    */
      public $table = 'reviews';
      public $fillable = ['products_id','reviews','product_rate','status','users_id'];
}
