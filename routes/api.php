<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app('Dingo\Api\Routing\Router'); // API With Dingo package

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*
* Api route for Controllers with Function 
* show , store ,destroy , index , update  .. Only
* 
*/
$api->version('v1', function ($api) {

/*
* Api route for ApiCategoriesController with Function 
* show , store ,destroy , index , update  .. Only
* 
*/
    $api->resource('categories', 'App\Http\Controllers\API\ApiCategoriesController',['only'=>['index','store','show','update','destroy']]);
/*
* Api route for ApiProductsController with Function 
* show , store ,destroy , index , update  .. Only
* 
*/

    $api->resource('products', 'App\Http\Controllers\API\ApiProductsController',['only'=>['index','store','show','update','destroy']]);
/*
* Api route for ApiHomeSliderController with Function 
* show , store ,destroy , index , update  .. Only
* 
*/
    $api->resource('homeslider', 'App\Http\Controllers\API\ApiHomeSliderController',['only'=>['index','store','show','update','destroy']]);
/*
* Api route for ApiHomeDataController with Function 
* show , store ,destroy , index , update  .. Only
* 
*/
    $api->resource('homedata', 'App\Http\Controllers\API\ApiHomeDataController',['only'=>['index','store','show','update','destroy']]);
/*
* Api route for ApiNormalUsersController with Function 
* show , store ,destroy , index , update  .. Only
* 
*/
    $api->resource('normalusers', 'App\Http\Controllers\API\ApiNormalUsersController',['only'=>['index','store','show','update','destroy']]);

/* Authitcation api for users */
    $api->post('normalusers/login', 'App\Http\Controllers\API\ApiNormalUsersController@login');

    $api->post('normalusers/logout', 'App\Http\Controllers\API\ApiNormalUsersController@logout');
/*
* Api route for ApiReviewsController with Function 
* show , store ,destroy , index , update  .. Only
* 
*/
    $api->resource('reviews', 'App\Http\Controllers\API\ApiReviewsController',['only'=>['index','store','show','update','destroy']]);

});

