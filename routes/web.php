<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/view_profile/{id}','UsersController@show'); // Get Data of User With Id for  Show/View 
Route::get('/edit_profile/{id}','UsersController@edit');// Get Data of User With Id for  Edit
Route::post('/edit_profile/{id}','UsersController@update');// Edit User Data 
