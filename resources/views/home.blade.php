@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          @if(isset($user))
            <div class="panel panel-default">
                <div class="panel-heading">Profil View</div>

                <div class="panel-body">
                 <div class="box-body" id="parent-form-area">
                     <style type="text/css">
                     #table-detail tr td:first-child {
                      font-weight: bold;
                      width: 25%;
                       }
                     </style>
                      <div class='table-responsive'>
                        <table id='table-detail' class='table table-striped'>
                            <tr><td>Name</td><td>{{ $user->name }}</td></tr>       
                            <tr><td>Email</td><td><a href='mailto:{{$user->email}}' target="_blank">{{$user->email}}</a></td></tr>           
                            <tr><td>Photo</td><td><a data-lightbox='roadtrip' href="{{asset('/'.$user->user_photo.'')}}"><img style='max-width:150px' title="Image For Photo" src="{{asset('/'.$user->user_photo.'')}}"/></a>
                            </td></tr>      
                            <tr><td>Phone Number</td><td>{{ $user->phone_number }}</td></tr>        
                        </table>
                     </div>  
                 </div><!-- /.box-body -->
                </div>
            </div>
            @elseif(isset($edit_user))
        <div class="panel panel-default">
           <div class="panel-heading">
             <strong><i class='fa fa-users'></i> Edit Profile </strong>
           </div> 
            <div class="panel-body" style="padding:20px 0px 0px 0px">
            <form class='form-horizontal' method='post' id="form"  action="{{ url('/edit_profile/'.$edit_user->id)}}" enctype="multipart/form-data"> 
                 
               @if(Session::has('message'))
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div>
                    @elseif(Session::has('error'))
                    <div class="alert alert-error">
                        {{ Session::get('error') }}
                    </div>
                @endif
                <div id="mail-status"></div>
                {{ csrf_field() }}

                <div class="box-body" id="parent-form-area">
                <div class='form-group header-group-0 ' id='form-group-name' style="">
                <label class='control-label col-sm-2'>Name <span class='text-danger' title='This field is required'></span></label>

                <div class="col-sm-9">
                <input type='text' title="Name" required class='form-control' name="name" id="name" value='{{$edit_user->name}}'/>
                                
               <div class="text-danger"></div>
               <p class='help-block'></p>
               </div>
               </div>  

                <div class='form-group header-group-0 ' id='form-group-email' style="">
                <label class='control-label col-sm-2'>Email <span class='text-danger' title='This field is required'></span></label>
                <div class="col-sm-9">
                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type='email' title="Email" required class='form-control' name="email" id="email" value='{{$edit_user->email}}'/>
                </div>                          
                <div class="text-danger"></div>
                <p class='help-block'></p>
                </div>
                </div>

                <div class='form-group header-group-0 ' id='form-group-mobile' style="">
                <label class='control-label col-sm-2'>Phone Number <span class='text-danger' title='This field is required'></span></label>
                <div class="col-sm-9">
                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type='text' title="Phone Number" required class='form-control' name="phone_number" id="phone_number" value='{{$edit_user->phone_number}}'/>
                </div>                          
                <div class="text-danger"></div>
                <p class='help-block'></p>
                </div>
                </div>

                <div class='form-group header-group-0 ' id='form-group-photo' style="">
                <label class='col-sm-2 control-label'>Photo <span class='text-danger' title='This field is required'></span></label>
                <div class="col-sm-9">
                <input type='file' id="photo" title="Photo"  class='form-control' name="photo"/>
                <div class="text-danger"></div>
                </div>
                </div>

                <div class='form-group header-group-0 ' id='form-group-password' style="">
                <label class='control-label col-sm-2'>Password </label>
                <div class="col-sm-9">
                <input type='password' title="Password" id="password" class='form-control' name="password"/>                           
                <div class="text-danger"></div>
                <p class='help-block'>Please leave empty if not change</p>
                </div>
                </div>                   
            </div><!-- /.box-body -->
                
            <div class="box-footer">         
             <div class="form-group">
             <label class="control-label col-sm-2"></label>
             <div class="col-sm-10 ">
             <input type="submit" name="submit" value='Save' class='btn btn-success'>                  </div>
             </div>                             
            </div><!-- /.box-footer-->

        </form><!-- /.form-->
        </div>
        </div>
            @else
        <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                   Welcom To Our Website .. !
                </div>
        </div>
            @endif
        </div>
    </div>
</div>
@endsection
